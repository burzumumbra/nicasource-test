/**
 * BLOCK: nicasource-block
 *
 * Registering a basic block with Gutenberg.
 * Simple block, renders and saves the same content without any interactivity.
 */
const { Component } = wp.element;
//  Import CSS.
import './editor.scss';
import './style.scss';
import * as api from './utils/api';
const { __ } = wp.i18n; // Import __() from wp.i18n
const { registerBlockType } = wp.blocks; // Import registerBlockType() from wp.blocks
class List extends Component {
	constructor(props) {
		super(...arguments);
		this.props = props;
		this.state ={
			products: []
		}
	}
	componentDidMount(){

		api.getProducts()
		.then((response)=>{
			console.log(response.data)
			this.setState({
				products: response.data
			});
		})
	}

	render(){
		return(
			<ul>
				{
					Object.values(this.state.products).map((product)=>{
						return(
						<li className="product">	
							{`Product: ${product.post_title}`}
							<span>{`Brand: ${product.Brand} - Expiration Date: ${product.ExpirationDate}`}</span>
							
						</li>
						)
					})
				}
			</ul>
		)
	}
}
/**
 * Register: aa Gutenberg Block.
 *
 * Registers a new block provided a unique name and an object defining its
 * behavior. Once registered, the block is made editor as an option to any
 * editor interface where blocks are implemented.
 *
 * @link https://wordpress.org/gutenberg/handbook/block-api/
 * @param  {string}   name     Block name.
 * @param  {Object}   settings Block settings.
 * @return {?WPBlock}          The block, if it has been successfully
 *                             registered; otherwise `undefined`.
 */
registerBlockType( 'cgb/block-nicasource-block', {
	// Block name. Block names must be string that contains a namespace prefix. Example: my-plugin/my-custom-block.
	title: __( 'nicasource-block' ), // Block title.
	icon: 'store', // Block icon from Dashicons → https://developer.wordpress.org/resource/dashicons/.
	category: 'common', // Block category — Group blocks together based on common traits E.g. common, formatting, layout widgets, embed.
	keywords: [
		__( 'nicasource-block' ),
		__( 'nicasource' ),
		__( 'create-guten-block' ),
	],

	/**
	 * The edit function describes the structure of your block in the context of the editor.
	 * This represents what the editor will render when the block is used.
	 *
	 * The "edit" property must be a valid function.
	 *
	 * @link https://wordpress.org/gutenberg/handbook/block-api/block-edit-save/
	 *
	 * @param {Object} props Props.
	 * @returns {Mixed} JSX Component.
	 */
	edit: ( props ) => {
		// Creates a <p class='wp-block-cgb-block-nicasource-block'></p>.
		let products = props.attributes.products;
		api.getProducts()
		.then((response)=>{
			console.log(response.data)
			props.setAttributes({products: response.data})
		})
		return (
			<div className={ props.className }>
				<h2>Product List</h2>
				<List />
			</div>
		);
	},

	/**
	 * The save function defines the way in which the different attributes should be combined
	 * into the final markup, which is then serialized by Gutenberg into post_content.
	 *
	 * The "save" property must be specified and must be a valid function.
	 *
	 * @link https://wordpress.org/gutenberg/handbook/block-api/block-edit-save/
	 *
	 * @param {Object} props Props.
	 * @returns {Mixed} JSX Frontend HTML.
	 */
	save: ( props ) => {
		console.log(props)
		return (
			<div className={ props.className }>
				<h2>Product List</h2>
				<ul>
				{
					props.attributes.products && (
						Object.values(props.attributes.products).map((product)=>{
							return(
							<li className="product">	
								{`Product: ${product.post_title}`}
								<span>{`Brand: ${product.Brand} - Expiration Date: ${product.ExpirationDate}`}</span>
							</li>
							)
						})
					)
					
				}
			</ul>
			</div>
		);
	},
} );
