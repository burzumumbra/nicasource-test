import axios from 'axios';


export const getProductsCategories = () => axios.get('/wp-json/nicasource/v1/categories');

export const getProducts = () => axios.get('/wp-json/nicasource/v1/products');
