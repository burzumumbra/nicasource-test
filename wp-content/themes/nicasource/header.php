<?php
/**
 * The header for the theme
 *
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
	
<a class="screen-reader-text" href="#content">Skip to content</a>

<header class="site-header">
	<?php
		$logo_header_url = get_template_directory_uri() . '/assets/images/logo.png';
		$logo_header_alt = get_bloginfo( 'name' );

		$toggle_normal = get_template_directory_uri() . '/assets/Menu-Button.png';
		$logo_output = '<div class="logo-container">';

		$logo_output .= '<a href="' . home_url() . '" rel="home nofollow">';
		$logo_output .= '<img class="logo" src="' . $logo_header_url . '" alt="' . $logo_header_alt . ' title="' . $logo_header_alt . ' - Página de Inicio" />';
		$logo_output .= '</a>';

		$logo_output .= '</div>';
		echo ( $logo_output );
	?>

	<nav class="main-navigation">
		<?php
		wp_nav_menu( array(
			'theme_location' => 'menu-1',
			'menu_id'        => 'primary-menu',
		) );
		?>
	</nav>
</header>

<div id="content" class="site-content">
	
