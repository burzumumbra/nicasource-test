<?php
/* Template Name: Home
*  Template Post Type: page
*/
get_header();
?>

<main id="Home" role="main">

	<?php

	if ( have_posts() ): ?>
        <div id="header">
            <h1><?php echo get_the_title(); ?></h1>
        </div>
        <div id="product_grid">
        <?php $query = new WP_Query( [
            'post_type'      => 'nica_product',
            'nopaging'       => true,
            'posts_per_page' => '3',
        ] ); ?>
        <?php $posts = $query->posts; ?>
        <?php if ( $query->have_posts() ) : ?>
            <?php while ( $query->have_posts() ) : $query->the_post(); ?>
                <?php 
                    $post_id            = get_the_ID();
                    $brand             = get_fields($post->ID)['brand'][0]->post_title;
                    $expiration_date    = get_fields($post->ID)['date_of_expiry'];
                    $excerpt = get_the_excerpt(); 

                    $excerpt = substr( $excerpt, 0, 70 ); // Only display first 260 characters of excerpt
                    $result = substr( $excerpt, 0, strrpos( $excerpt, ' ' ) );
                ?>
                <div class="product">
                    <span class="exp"><i class="far fa-calendar"></i> Exp: <?php echo $expiration_date; ?></span>
                    <a class="title" href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
                        <h2><?php the_title(); ?></h2>
                    </a>
                    <p class="excerpt"><?php echo ( $result . '...'); ?></p>
                    <span class="brand"><?php echo $brand;?></span>
                </div>

            <?php endwhile; ?>
        <?php endif; ?>

        <?php wp_reset_postdata(); ?>
        </div>
	<?php	
	endif;

	?>

</main><!-- #site-content -->

<?php get_footer(); ?>