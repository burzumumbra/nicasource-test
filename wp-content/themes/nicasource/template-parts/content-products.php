<?php
/**
 * Template part for displaying products
 */

?>

<article id="post-<?php the_ID(); ?>" class="entry">
	<?php 
		$post_id            = get_the_ID();
		$brand             	= get_fields($post->ID)['brand'][0]->post_title;
		$expiration_date    = get_fields($post->ID)['date_of_expiry'];
		$term_obj_list 		= get_the_terms( $post->ID, 'product_categories' );
		$terms_string 		= join(', ', wp_list_pluck($term_obj_list, 'name'));
		
	?>
	<header class="entry-header">
		<h1><span><?php echo $brand;?></span><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h1>
		<div class="details">
			<span><i class="fas fa-archive"></i><?php echo $terms_string; ?></span>
			<span><i class="far fa-calendar"></i><?php echo $expiration_date; ?></span>
		</div>
	</header>

	<div class="entry-content">
		<?php the_content(); ?>
	</div>
</article>