# NicaSource Test Project

The repository includes code for the plugin, guttenberg block plugin and the theme for this test.

# Plugin - Custom post types

The plugin is at the directory: *wp-content/plugins/catalog-nicasource* 


# Guttenberg Block Plugin

The plugin is at the directory: *wp-content/plugins/nicasource-block* 
To run or build: 

   >npm run start or npm run build 

This block use the repository https://github.com/ahmadawais/create-guten-block as starter block generator.
## Switch to another file


# REST API Endpoints
The followings are the endpoints for products, brands, and categories from the custom taxonomy.
**Products:**
> /wp-json/nicasource/v1/products
> /wp-json/nicasource/v1/products?category=test
> /wp-json/nicasource/v1/products?brand=random-brand

**Brands:**
> /wp-json/nicasource/v1/brands

**Categories:**
> /wp-json/nicasource/v1/categories


# Theme

The theme was using repository https://github.com/taniarascia/untheme as starter 
and the bundler manager https://wpack.io/ 
To run locally first it will need to run:
> composer require wpackio/enqueue

To Watch, build or zip the theme use the followings commands:

> npm run start
> npm run build
> npm run archive
